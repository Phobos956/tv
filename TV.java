/**
 * <p> Filename: TV.java </p>
 * <p> Creation date: 04/02/2020 </p>
 * <p> Last modified: 09/02/2020 </p>
 * <p> Purpose: Class to handle the TVs in the Museum </p>
 * @author Ventsislav Yordanov
 * @version 1.0.2
 * @since 1.0.0
 */

public class TV
{
	private String power;//Current power state of the TV.
	private int channelNum;//Current channel.
	private int volume;//Current volume.
	private int tvCounter;//Number of created TV objects.
	private static int SERIAL_NUM = 1000;//Starts at 1000, and will be incremented by one, every
									    //time a new TV object is created.
	private int serialNum;//Holds the unique serial number of every created TV object.
	private int channelLimit;//Current max number of channels a TV object has.
	private final String POWER_OFF_ERROR = "Error: TV is off";
	private final String CHANNEL_OPERATION = " while trying to perform a channel operation";
	
	
	/**
	* @param power Set to "On" or "Off".
	* @param channel Set the channel.
	* @param volume Set the volume.
	*/
	public TV(String power, int channelLimit)
	{   
		this.channelLimit = channelLimit;
		this.power = power;
		channelNum = 1;
		volume = 50;
		//TV.SERIAL_NUM++;
		serialNum = TV.SERIAL_NUM++;
	}
	
	/**
	 * Creates a TV with: power = "Off", channelLimit = 10, volume = 50, channelNum = 1.
	 */
	public TV()
	{
		channelLimit = 10;
		power = "Off";
		channelNum = 1;
		volume = 50;
		serialNum = TV.SERIAL_NUM++;
	}
	
	/**
	 * Creates a TV with: power = "Off", volume = 50, ChannelNum = 1.
	 * @param channelLimit Sets the max number of channels.
	 */
	public TV(int channelLimit)
	{
		this.channelLimit = channelLimit;
		power = "Off";
		channelNum = 1;
		volume = 50;
		serialNum = TV.SERIAL_NUM;
	}
	
	/**Method for the power state. It can be toggled on or off.*/
	public void power()
	{
		if(power.equals("Off"))
		{
			power = "On";
		}
		else if(power.equals("On"))
		{
			power = "Off";
		}
	}
	
	/**
	 * Setter for channelNum. Also performs checks on: power state, channel validity.
	 * @param newChannelNum Sets the new channel.
	 */
	public void setChannel(int newChannelNum)
	{
		/**Checks if TV is on or off. If its off - outputs an error msg.*/
		if(power.equals("Off"))
		{
			System.out.println(POWER_OFF_ERROR + CHANNEL_OPERATION);
		}
		else
		{
			/**throws IllegalArgumentException if channel is invalid.*/
			if(newChannelNum >= 1 && newChannelNum <= channelLimit)
			{
				channelNum = newChannelNum;
			}
			else
			{
				throw new IllegalArgumentException("Invalid channel number");
			}
		}
	}
	
	/**
	 * Getter for channelNum. Also performs checks on: power state.
	 * @return If TV is Off - Error.
	 * <p> If TV is ON - current channel. </p>
	 */
	public int getChannel()
	{
		/**Checks if TV is on or off. If its off - outputs an error msg.*/
		if(power.equals("Off"))
		{
			throw new IllegalArgumentException("TV is off: cannot get channel.");
		}
		else
		{
			return channelNum;
		}
	}
	
	/**Method for switching to the next channel. Also performs checks on: power state. Loops round to
	 * channel 1, if channelLimit is reached.*/
	public void nextChannel()
	{
		/**Checks if TV is on or off. If its off - outputs an error msg.*/
		if(power.equals("Off"))
		{
			System.out.println("Error: TV is off, cannot select next channel");
		}
		else
		{
			/**If channelLimit is reached, cycle round to channel 1.*/
			if (channelNum == channelLimit)
			{
				channelNum = 1;
			}
			/**If channelLimit is not reached, go to next channel.*/
			else if (channelNum < channelLimit)
			{
				channelNum++;
			}
		}
	}
	/** Method switching to previous channel. Also performs checks on: power state. Loops round to
	*the last channel, if channel 1 is reached.*/
	public void prevChannel()
	{
		/**Checks if TV is on or off. If its off - outputs an error msg.*/
		if(power.equals("Off"))
		{
			System.out.println("Error: TV is off, cannot select previous channel");
		}
		else
		{
			/**Loop round if channelLimit is reached, else go to previous channel.*/
			if (channelNum == 1)
			{
				channelNum = channelLimit;
			}
			else if (channelNum > 1)
			{
				channelNum--;
			}
		}
	}
	
	/** Method for increasing the volume.
	 * The volume is capped and should always be within the range of 0 to 100. Every time this
	 * method is called, volume is increased by 2%.
	 */
	public void increaseVolume()
	{
		/**Checks if TV is on or off. If its off - outputs an error msg.*/
		if(power.equals("Off"))
		{
			System.out.println("Error: TV is off, cannot increase volume");
		}
		else
		{
			/**If volume < 100, increase by 2, else throw an exception.*/
			if (volume < 100)
			{
				volume = volume + 2;
			}
			else
			{
				throw new IllegalArgumentException("Max volume");
			}
		}
	}
	/** Method for decreasing the volume.
	 * The volume is capped and should always be within the range of 0 to 100. Every time this
	 * method is called, volume is decreased by 2%.
	 */
	public void decreaseVolume()
	{
		/**Checks if TV is on or off. If its off - outputs an error msg.*/
		if(power.equals("Off"))
		{
			System.out.println("Error: TV is off, cannot decrease volume");
		}
		else
		{
			/**If volume > 0, decrease by 2, else throw an exception.*/
			if (volume > 0)
			{
				volume = volume - 2;
			}
			else
			{
				throw new IllegalArgumentException("Mute");
			}
		}
	}
	/**
	 * Getter for volume. Also performs checks on: power state.
	 * @return If TV is Off - Error.
	 * <p> If TV is ON - current volume. </p>
	 */
	public int getVolume()
	{
		if(power.equals("Off"))
		{
			throw new IllegalArgumentException("TV is off: cannot get volume.");
		}
		else
		{
			return volume;
		}
	}
	
	/**
	 * Getter for power. Also performs checks on: power state.
	 * @return Power state.
	 */
	public String getPower()
	{
		if(power.equals("Off"))
		{
			return "TV is " + power;
		}
		else
		{
			return "TV is " + power;
		}
	}
	
	/**
	 * @return Serial Number.
	 */
	public int getSerialNum()
	{
		return serialNum;
	}
	
	/**
	 * Getter for channelLimit. Also performs checks on: power state.
	 * @return If TV is Off - Error.
	 * <p> If TV is ON - Max number of channels. </p>
	 */
	public int getChannelLimit()
	{
		if(power.equals("Off"))
		{
			throw new IllegalArgumentException("TV is off: cannot get ChannelLimit.");
		}
		else
		{
			return channelLimit;
		}
	}

}
